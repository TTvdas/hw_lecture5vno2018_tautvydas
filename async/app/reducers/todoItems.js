export default (state = { items: [] }, action) => {
  switch (action.type) {
    case 'ADD_NEW':
      return {
        ...state,
        items: [
          ...state.items,
          action.todo
        ]
      };
    case 'LOAD_TODOS':
      return {
        ...state, items: action.todos, loading: false,
      };

    case 'DELETE_TODO':
      return {
        ...state,
        items: state.items.filter(item => item.id !== action.id),
      };

    case 'EDIT_TODO':
      return {
        ...state,
        items: state.items.map(
          (item) => item.id === action.id ? { ...item, title: action.title }
            : item
        )
      };

      case 'TOGGLE_TODO':
      return {
        ...state,
        items: state.items.map(
          (item) => item.id === action.id ? { ...item, completed: !(action.completed) }
            : item
        )
      };

    case 'LOADING':
      return {
        ...state, loading: true,
      };
    default:
      return state;
  }
};
