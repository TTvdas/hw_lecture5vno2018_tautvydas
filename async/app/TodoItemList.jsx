import React from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TodoItem from './TodoItem';
import * as actions from './actions';
import PropTypes from 'prop-types';
import { Input } from 'material-ui';

class TodoItemList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: ''
    };
    this.props.loadItems();
  }

  addTodoItem(e) {
    e.preventDefault();
    if (this.state.inputValue !== '') {
      const title = this.state.inputValue;
      this.setState({ inputValue: '' });
      this.props.dispatchAddNewItem(title);
    }
  }

  deleteItem(itemId) {
    let newList = this.state.todoItemList.filter(item => item.id !== itemId);
    this.setState({ todoItemList: newList });
  }

  inputChanged(e) {
    this.setState({
      inputValue: e.target.value
    });
  }

  render() {
    const { todoItems } = this.props;

    const items = todoItems.map(item => {
      return <TodoItem
        key={item.id}
        id={item.id}
        title={item.title}
        deleteItem={this.props.deleteItem}
        editItem={this.props.editItem}
        toggleItem={this.props.toggleItem}
      />
    });

    return (
      <div>
        <h1>{this.props.header}</h1>
        <form onSubmit={this.addTodoItem.bind(this)}>
          <Input value={this.state.inputValue}
            type="text"
            onChange={this.inputChanged.bind(this)}
            value={this.state.inputValue} />
        </form>
        {this.props.loading && <h1>The items are loading...</h1>}
        <div>
          <ul>
            {items}
          </ul>
        </div>
      </div>
    );
  }
}

TodoItemList.propTypes = {
  todoItems: PropTypes.any,
  loading: PropTypes.any,

  dispatchAddNewItem: PropTypes.any,
  loadItems: PropTypes.any,
  deleteItem: PropTypes.any,
  editItem: PropTypes.any,
  toggleItem: PropTypes.any,
}

export default connect(
  (state) => ({
    todoItems: state.todoItems.items,
    loading: state.todoItems.loading,
  }),
  (dispatch) => bindActionCreators({
    dispatchAddNewItem: actions.addNewItem,
    loadItems: actions.loadItems,
    deleteItem: actions.deleteItem,
    editItem: actions.editItem,
    toggleItem: actions.toggleItem,
  }
    , dispatch)
)(TodoItemList)