const defaultAppSettings = {
    loading: false,
    appColor: '#BADA55'
};

const storeItem = (itemType, item) => {
    localStorage.setItem(itemType, JSON.stringify(item));
};

const getItem = itemType => JSON.parse(localStorage.getItem(itemType));

const imitateApiDelay = contentToServe => new Promise((resolve) => {
    setTimeout(() => resolve(contentToServe), 500);
});

export default {
    updateAppSettings(appSettings) {
        storeItem('appSettings', appSettings);
        return imitateApiDelay(appSettings);
    },

    getAppSettings() {
        const appSettings = getItem('appSettings') || defaultAppSettings;
        return imitateApiDelay(appSettings);
    },

    getTasks() {
        const appTodos = getItem('appTodos') || [];
        return imitateApiDelay(appTodos);
    },

    getAppTodoById(id) {
        const appTodos = getItem('appTodos') || [];

        const todoById = appTodos.find(todo => todo.id === id);

        return imitateApiDelay({
            success: true,
            todo: todoById
        });
    },

    addAppTodo(title) {
        const appTodos = getItem('appTodos') || [];

        //id cannot start with a number, su add a letter at the beginning 
        const newId = 'tdi-' + Math.random().toString(36).substring(2, 15); 

        appTodos.push({
            id: newId,
            title,
            completed: false
        });

        storeItem('appTodos', appTodos);

        return imitateApiDelay(newId);
    },

    removeAppTodo(id) {
        const appTodos = getItem('appTodos') || [];

        const filteredTodos = appTodos.filter(todo => todo.id !== id);

        storeItem('appTodos', filteredTodos);

        return imitateApiDelay({ success: true });
    },

    toggleAppTodo(id) {
        const appTodos = getItem('appTodos') || [];

        const toggledTodos = appTodos.map((todo) => {
            if (todo.id === id) {
                return {
                    ...todo,
                    completed: !todo.completed
                };
            }
            return todo;
        });

        storeItem('appTodos', toggledTodos);

        return imitateApiDelay({ success: true });
    },

    editTodoTitle(id, newTitle) {
      const appTodos = getItem('appTodos') || [];

      const toggledTodos = appTodos.map((todo) => {
          if (todo.id === id) {
              return {
                  ...todo,
                  title: newTitle
              };
          }
          return todo;
      });

      storeItem('appTodos', toggledTodos);

      return imitateApiDelay({ success: true });
  }
};
