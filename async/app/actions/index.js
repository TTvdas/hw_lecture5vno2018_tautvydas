import api from '../server/remoteApi';

export const changeColor = (colorHexCode) => {
    return {
        type: 'CHANGE_COLOR',
        colorHexCode
    }
};

export const addNewItem = title => (dispatch) => {
    api.addAppTodo(title)
        .then((newTodoId) => {
            api.getAppTodoById(newTodoId)
                .then((response) => {
                    dispatch({
                        type: 'ADD_NEW',
                        todo: response.todo
                    });
                });
        });
};

export const loadItems = () => (dispatch) => {
  dispatch({
    type: 'LOADING',
  })
  api.getTasks()
    .then((todos) => {
      dispatch({
        type: 'LOAD_TODOS',
        todos: todos,
      });
    });
}

export const deleteItem = (id) => (dispatch) => {
  api.removeAppTodo(id)
    .then((response) => {
      dispatch({
        type: 'DELETE_TODO',
        id: id,
      });
    });
}

export const editItem = (id, newTitle) => (dispatch) => {
  api.editTodoTitle(id, newTitle)
  .then((response) => {
    dispatch({
      type: 'EDIT_TODO',
      id: id,
      title: newTitle,
    });
  });
}

export const toggleItem = (id) => (dispatch) => {
  api.toggleAppTodo(id)
  .then((response) => {
    dispatch({
      type: 'TOGGLE_TODO',
      id: id,
    });
  });
}