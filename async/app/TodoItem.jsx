import React from 'react';
import Modal from 'react-modal';
import IconButton from 'material-ui/IconButton';
import DeleteIcon from 'material-ui-icons/Delete';
import EditIcon from 'material-ui-icons/Edit';
import { Checkbox } from 'material-ui'
import PropTypes from 'prop-types';
import { Input } from 'material-ui';
import Button from 'material-ui/Button';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};

Modal.setAppElement('.react-app');

class TodoItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggled: false,
      modalIsOpen: false,
      inputValue: '',
    }
    this.toggleTodo = this.toggleTodo.bind(this);
    this.editTodo = this.editTodo.bind(this);

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.inputChanged = this.inputChanged.bind(this);
  }

  inputChanged(e) {
    this.setState({
      inputValue: e.target.value
    });
  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {
    this.setState({
      inputValue: this.props.title
    });
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  toggleTodo() {
    this.props.toggleItem(this.props.id)
    this.setState({
      toggled: !this.state.toggled,
    });
  }

  editTodo(e) {
    e.preventDefault();
    this.props.editItem(this.props.id, this.state.inputValue);
    this.setState({
      modalIsOpen: false,
    });
  }

  render() {
    return (
      <div>
        <li className={this.state.toggled ? 'todo-item line-through' : 'todo-item'}>
          {this.props.title}
          {
            !this.state.toggled &&
            <IconButton className="icon" color="primary" aria-label="Delete" onClick={() => this.props.deleteItem(this.props.id)}>
              <DeleteIcon />
            </IconButton>
          }
          <Checkbox className="icon" checked={this.state.toggled} onChange={this.toggleTodo} />
          {
            !this.state.toggled &&
            <IconButton className="icon" color="primary" aria-label="Delete" onClick={this.openModal}>
              <EditIcon />
            </IconButton>
          }
        </li>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Edit the todo"
        >
          <form className="form" onSubmit={this.editTodo}>
            <h2 className="form-field">Change the title:</h2>
            <Input value={this.state.inputValue}
              className="form-field"
              type="text"
              onChange={this.inputChanged}
              value={this.state.inputValue} />
            <Button className="form-field" variant="raised" color="primary" type="submit">
              change
            </Button>
          </form>
        </Modal>
      </div>
    );
  }
}

TodoItem.propTypes = {
  id: PropTypes.any,
  title: PropTypes.any,
  deleteItem: PropTypes.any,
  editItem: PropTypes.any,
  toggleItem: PropTypes.any,
}

export default TodoItem;